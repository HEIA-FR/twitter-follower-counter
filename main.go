// Copyright 2018 Jacques Supcik
// Haute école d'ingénierie et d'architecture de Fribourg
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"flag"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/ChimeraCoder/anaconda"
	MQTT "github.com/eclipse/paho.mqtt.golang"
	log "github.com/sirupsen/logrus"
)

const (
	fetchPeriod = 2 * time.Second
)

func getEnvOrPanic(key string) string {
	val, ok := os.LookupEnv(key)
	if !ok {
		log.Panicf("Environment variable %v not defined", key)
	}
	return val
}

func main() {
	debug := flag.Bool("debug", false, "Run in debug mode")
	verbose := flag.Bool("v", false, "Run in verbose mode")
	broker := flag.String("broker", "tcp://127.0.0.1:1883", "MQTT Broker URI")
	user := flag.String("username", "", "MQTT Username")
	password := flag.String("password", "", "MQTT Password")
	topic := flag.String("topic", "message", "Topic name from which to subscribe")
	qos := flag.Int("qos", 0, "Quality of Service 0,1,2 (default 0)")
	tuser := flag.String("tuser", "HEIAFR", "Twitter User")

	flag.Parse()

	if *debug {
		log.SetLevel(log.DebugLevel)
	} else if *verbose {
		log.SetLevel(log.InfoLevel)
	} else {
		log.SetLevel(log.WarnLevel)
	}

	consumerKey := getEnvOrPanic("TWITTER_CONSUMER_KEY")
	consumerSecret := getEnvOrPanic("TWITTER_CONSUMER_SECRET")
	accessToken := getEnvOrPanic("TWITTER_ACCESS_TOKEN")
	accessSecret := getEnvOrPanic("TWITTER_ACCESS_SECRET")

	log.Debugln("Setting Twitter Keys")
	anaconda.SetConsumerKey(consumerKey)
	anaconda.SetConsumerSecret(consumerSecret)
	log.Debugln("Creating Twitter API")
	api := anaconda.NewTwitterApi(accessToken, accessSecret)

	log.Debugln("Creating MQTT Options")
	opts := MQTT.NewClientOptions()
	opts.AddBroker(*broker)
	opts.SetUsername(*user)
	opts.SetPassword(*password)

	log.Debugln("Creating MQTT Client")
	client := MQTT.NewClient(opts)
	log.Debugln("Connecting MQTT Client")
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		log.Panic(token.Error())
	}

	go func() {
		log.Infoln("Starting main loop")
		ticker := time.NewTicker(fetchPeriod)
		for range ticker.C {
			log.Debugln("Getting user info")
			user, err := api.GetUsersShow(*tuser, nil)
			if err != nil {
				log.Errorf("Error reading user info : %v", err)
				continue
			}
			log.Debugf("Follower count : %v", user.FollowersCount)
			client.Publish(*topic, byte(*qos), true, strconv.Itoa(user.FollowersCount))
		}
	}()

	// Wait for signal
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	<-sigs
	log.Infoln("Terminated")
}
